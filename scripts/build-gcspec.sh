#!/bin/bash

IMAGE=action-java-v11

CMD_GC='CMD ["java", "-Xquickstart", "-Xms2048m", "-Xmx2048m", "-cp", "/ow_runtime/lib/gson-2.8.7.jar:/ow_runtime/build", "com.mattwelke.openwhisk.runtime.java.action.Proxy"]'
CMD_NOGC='CMD ["java", "-Xquickstart", "-Xms2048m", "-Xmx2048m", "-Xgcpolicy:nogc", "-cp", "/ow_runtime/lib/gson-2.8.7.jar:/ow_runtime/build", "com.mattwelke.openwhisk.runtime.java.action.Proxy"]'

if [ -z "$1" ]; then
    echo "Usage: $0 NAMESPACE [{gc|nogc} [TAG]]"
    echo
    echo "* NAMESPACE: Docker namespace of the built image"
    echo "* gc|nogc: build with garbage collector enable or disabled (optional, default: gc)"
    echo "* TAG: override default tag ("gc" or "nogc", depending on previous parameter; or "latest" if not passed)"

    exit 2
fi >&2


case "$2" in
    "gc")
        sed 's!^CMD.*!'"$CMD_GC"'!' Dockerfile > Dockerfile.gc
        dockerfile="Dockerfile.gc"
        customTag="$2";;
    "nogc")
        sed 's!^CMD.*!'"$CMD_NOGC"'!' Dockerfile > Dockerfile.nogc
        dockerfile="Dockerfile.nogc"
        customTag="$2";;
    *)
        dockerfile=Dockerfile
        customTag="latest";;
esac

if [ -n "$3" ]; then
    customTag="$3"
fi

# Compile Java source code files.
javac \
  -cp 'lib/gson-2.8.7.jar' \
  -d build \
  src/com/mattwelke/openwhisk/runtime/java/action/*.java \
  src/org/apache/openwhisk/runtime/java/action/*.java \
|| exit 1

# Build Docker image with compiled Java class files and dependency JAR files.
docker build -f "$dockerfile" -t "$1"/$IMAGE:"$customTag" .
